import React from "react";
import ReactDOM from "react-dom";
import { Formik, Form, useField } from 'formik';
import "./styles.css";
import * as Yup from 'yup';
import "yup-phone";
import face from './Images/huma_glasses.png';
const MyTextInput = ({ label, ...props }) => {
    // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
    // which we can spread on <input>. We can use field meta to show an error
    // message if the field is invalid and it has been touched (i.e. visited)
    const [field, meta] = useField(props);
    return (
      <>
        <label htmlFor={props.id || props.name}>{label}</label>
        <input className="text-input" {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </>
    );
  };
  
  
  // And now we can use these
  const SignupForm = () => {
    return (
      <body className = "background-layer">
        <div className = "side-bars"></div>
          <div className = "inner-background">
          <h3 style= {{fontSize: 36, fontFamily: "Arial", width:500, paddingBottom:25}}>Welcome to Human Managed!</h3>
          <div style={{paddingLeft:60}}>
            <Formik
              initialValues={{ 
                firstName: '',
                lastName: '',
                email: '',
                HP: '',
                Company: '',
                jobTitle: '',
              }}
              validationSchema={Yup.object({
                firstName: Yup.string()
                  .max(15, 'Must be 15 characters or less')
                  .required('Required'),
                lastName: Yup.string()
                  .max(20, 'Must be 20 characters or less')
                  .required('Required'),
                email: Yup.string()
                  .email('Invalid email address')
                  .required('Required'),
                HP: Yup.string()
                    .phone()//uses the yup-phone library to validate the phone number
                    .required('Required'),
                Company: Yup.string()
                    .max(20, 'Must be 20 characters or less')
                    .required('Required'),
                jobTitle: Yup.string()
                    .max(20, 'Must be 20 characters or less')
                    .required('Required'),
              })}
              onSubmit={(values, { setSubmitting }) => {
               
                console.log('Form data', values);
                setTimeout(() => {
                  alert(JSON.stringify(values, null, 2));
                  setSubmitting(false);
                }, 400);
              }}
            >
            {( { values, handleChange, handleBlur }) => (
                <Form>             
                <MyTextInput
                  label=""
                  name="firstName"
                  type="text"
                  placeholder="FIRST NAME"        
                />
                <MyTextInput
                label=""
                  name="lastName"
                  type="text"
                  placeholder="LAST NAME"
                />
                <MyTextInput
                label=""
                  name="email"
                  type="email"
                  placeholder="E-MAIL"
                />
                <MyTextInput
                  label=""
                  name="HP"
                  type="text"
                  placeholder="PHONE NUMBER"
                />
                <MyTextInput
                  label=""
                  name = "Company"
                  type="text"
                  placeholder="COMPANY NAME"
                />
                <MyTextInput
                  name="jobTitle"
                  type="text"
                  placeholder="JOB TITLE"
                />
                <img src={face} alt = "" className="image"/>
                <br/>
                <button type="submit">C R E A T E</button>
              </Form>
            )}
            </Formik>          
          <div>
        
        </div>
          
        </div> 
      </div>
      </body>
    );
  };

function App() {
  return <SignupForm />;
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
